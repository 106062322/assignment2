const GAME_WIDTH = 1100;
const GAME_HEIGHT = 750;

const DISPLAY_WIDTH = 764; // 操作區域
const DISPLAY_HEIGHT = 750;

const scores = [];
let isClicked = false;
let isScoreProcess = true;

const menuState = {
  preload: function() {
    game.load.image('background', 'assets/background.png');
    game.load.image('menu', 'assets/menu.png');
    game.load.image('score_board_panel', 'assets/score_board_panel.png');

    game.load.image('start_game', 'assets/start_game.png');
    game.load.image('start_game_hover', 'assets/start_game_hover.png');

    game.load.image('score_board', 'assets/score_board.png');
    game.load.image('score_board_hover', 'assets/score_board_hover.png');
  },
  create: function() {
    game.stage.backgroundColor = '#fff';

    // Background Setting
    this.backgroundOne = game.add.sprite(0, 0, 'background'); 
    game.physics.arcade.enable(this.backgroundOne);
    this.backgroundOne.body.velocity.y = 200;

    this.backgroundTwo = game.add.sprite(0, -1563, 'background'); 
    game.physics.arcade.enable(this.backgroundTwo);
    this.backgroundTwo.body.velocity.y = 200;

    this.menu = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT / 2, 'menu');
    this.menu.anchor.setTo(0.5, 0.5);

    this.scoreBoardPanel = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT / 2, 'score_board_panel');
    this.scoreBoardPanel.anchor.setTo(0.5, 0.5);
    this.scoreBoardPanel.visible = false;

    this.startGame = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT/2 - 20, 'start_game');
    this.startGame.anchor.setTo(0.5, 0.5);

    this.startGameHover = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT/2 - 20, 'start_game_hover');
    this.startGameHover.anchor.setTo(0.5, 0.5);
    this.startGameHover.alpha = 0;
    this.startGameHover.inputEnabled = true;

    this.startGameHover.events.onInputDown.add(this.startGameHandler, this);

    this.scoreBoard = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT/2 + 70, 'score_board');
    this.scoreBoard.anchor.setTo(0.5, 0.5);

    this.scoreBoardHover = game.add.sprite(DISPLAY_WIDTH / 2, GAME_HEIGHT/2 + 70, 'score_board_hover');
    this.scoreBoardHover.anchor.setTo(0.5, 0.5);
    this.scoreBoardHover.alpha = 0;
    this.scoreBoardHover.inputEnabled = true;

    this.scoreBoardHover.events.onInputDown.add(this.toggleState, this);

    this.score = [];
    this.mock = [];
    this.backspaceButton = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);

    firebase.database().ref('score').once('value', (snapshot) => {
      const scoreList = snapshot.val();
      for (const key in scoreList) {
        scores.push(scoreList[key]);
      }
      for (let i = scores.length; i < 3; i++) {
        scores.push(0);
      }
      scores.sort((a, b) => b-a);
      isScoreProcess = false;
      if (this.scoreBoardPanel.visible) this.renewScores();
    });
  },
  update: function() {
    if (this.startGameHover.input.pointerOver()) this.startGameHover.alpha = 1;
    else this.startGameHover.alpha = 0;

    if (this.scoreBoardHover.input.pointerOver()) this.scoreBoardHover.alpha = 1;
    else this.scoreBoardHover.alpha = 0;

    if (this.backspaceButton.isDown && !isClicked) {
      isClicked = true;
      this.toggleState();
    }

    if (this.backspaceButton.isUp) {
      isClicked = false;
    }

    this.backgroundCombine();
  },
  backgroundCombine: function() {
    if (this.backgroundOne.y >= 750) {
        this.backgroundOne.y -= 1563*2;
    }
    if (this.backgroundTwo.y >= 750) {
        this.backgroundTwo.y -= 1563*2;
    }
  },
  startGameHandler: function() {
    game.state.start('main');
  },
  toggleState: function() {
    this.menu.visible = !this.menu.visible;
    this.scoreBoardPanel.visible = !this.scoreBoardPanel.visible;
    
    this.startGame.visible = !this.startGame.visible;
    this.startGameHover.visible = !this.startGameHover.visible;
    this.scoreBoard.visible = !this.scoreBoard.visible;
    this.scoreBoardHover.visible = !this.scoreBoardHover.visible;

    this.renewScores();
  },
  renewScores: function() {
    // Display scores
    if (this.scoreBoardPanel.visible) {
      if (isScoreProcess) {
        this.mock[0] = game.add.text(400, 235, '...', { font: '45px Arial', fill: '#91d4d5' });
        this.mock[1] = game.add.text(400, 345, '...', { font: '45px Arial', fill: '#91d4d5' });
        this.mock[2] = game.add.text(400, 455, '...', { font: '45px Arial', fill: '#91d4d5' });
      } else {
        this.score[0] = game.add.text(400, 235, scores[0], { font: '45px Arial', fill: '#91d4d5' });
        this.score[1] = game.add.text(400, 345, scores[1], { font: '45px Arial', fill: '#91d4d5' });
        this.score[2] = game.add.text(400, 455, scores[2], { font: '45px Arial', fill: '#91d4d5' });
      }
    }
    else {
      for (let item of this.score) {
        item.destroy();
      }
      for (let item of this.mock) {
        item.destroy();
      }
    }
  }
};

var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.start('menu');